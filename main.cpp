#include <iostream>
#include "Swiat.h"

int main()
{
	CSwiat *swiat = new CSwiat();
	char user_input = 'n';
	bool simulate = true;

	while(simulate)
	{
		if (user_input == 'q' || user_input == 'Q')
		{
			delete swiat;
			simulate = false;
		}
		else if (user_input == 'n' || user_input == 'N')
			swiat->wykonajTure();
		user_input = std::cin.get();
	}

	return 0;
}