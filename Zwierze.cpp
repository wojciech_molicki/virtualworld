#include "Zwierze.h"
#include "Swiat.h"

//class CSwiat;

CZwierze::CZwierze(CSwiat *swiat, int x, int y) : COrganizm(swiat, x, y)
{
	_rozmnazalSie = false;
}

CZwierze::~CZwierze(void)
{
}

void CZwierze::akcja() {
	// jesli nie jest zywy, akcja nic nie robi
	if (getJestZywy() == false)
		return;

	//wybierz losowo jedno z sasiednich pol
	int pole = wybierzLosowePole();
	int x = pole % 20; int y = pole / 20;
	//jesli kolizja to odpal kolizje
	COrganizm *kolidujacy = _swiat->czyKolizja(x, y);
	if (kolidujacy != nullptr)
	{
		//printf("kolizja %d,%d na %d,%d\n", getX(), getY(), x, y);
		kolizja(kolidujacy);
		kolidujacy->kolizja(this);
	}
	else
	{
		setXY(x, y);
	}
}

void CZwierze::kolizja(COrganizm *o) {
	if (_symbol == o->getSymbol()) {
		//rozmnazanie
		int pole = wybierzLosoweWolnePole();
		//wybierz wolne pole, jesli brak to nie rozmnazaj
		if (pole != -1 && _rozmnazalSie == false)
		{
			_swiat->dodajOrganizm(getSymbol(), pole % 20, pole / 20);
			_swiat->komunikat("Rozmnazanie organizmow typu: " + getName());
			_rozmnazalSie = true;
		}
	}
	else {
		//walka
		if (getSila() > o->getSila())
			o->setJestZywy(false);
		else if (getSila() == o->getSila())
		{
			o->setJestZywy(false);
		}
		else
			setJestZywy(false);
		if (getJestZywy())
		{
			_swiat->komunikat("[ATAK]: " + getName() + " => [OBRONA]: " +
				o->getName() + " | ginie: " + o->getName());
			setXY(o->getX(), o->getY());
		}
		else
		{
			_swiat->komunikat("[ATAK]: " + getName() + " => [OBRONA]: " +
				o->getName() + " | ginie: " + getName());
			//gdy ginie atakujacy, broniacy zostaje na miejscu
			//o->setXY(getX(), getY());
		}
	}
}