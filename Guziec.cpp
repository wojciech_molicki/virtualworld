#include "Guziec.h"
#include "Swiat.h"

CGuziec::CGuziec(CSwiat *swiat, int x, int y) : CZwierze(swiat, x, y)
{
	_symbol = 'U';
	_sila = 3;
	_inicjatywa = 6;
	_bonusSila = 0;
}


CGuziec::~CGuziec(void)
{
}

void CGuziec::kolizja(COrganizm *o) {
	CZwierze::kolizja(o);

	//guziec z kazda potyczka staje sie silniejszy
	if (o->getJestZywy() == false) 
	{
		_bonusSila++;
		_swiat->komunikat("Guziec staje sie silniejszy!");
	}
}

int CGuziec::getSila() {
	return _sila + _bonusSila;
}

void CGuziec::akcja() {
   CZwierze::akcja();
}
