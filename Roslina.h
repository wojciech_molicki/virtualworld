#pragma once
#include "organizm.h"
class CRoslina :
	public COrganizm
{
public:
	CRoslina(CSwiat *swiat, int x, int y);
	~CRoslina(void);

	virtual void akcja();
	virtual void kolizja(COrganizm *o);
protected:
	double _pRozmnazania;
};
