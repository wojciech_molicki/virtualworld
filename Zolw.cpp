#include "Zolw.h"
#include "Swiat.h"

CZolw::CZolw(CSwiat *swiat, int x, int y) : CZwierze(swiat, x, y)
{
	_symbol = 'Z';
	_sila = 2;
	_inicjatywa = 1;
}

CZolw::~CZolw(void)
{
}

void CZolw::akcja()
{
	if (getJestZywy() == false)
		return;

	int losowa = rand() % 101;
	if (losowa > 75)
	{
		_swiat->komunikat("Zolw raczyl sie ruszyc!");
		CZwierze::akcja();
	}
}