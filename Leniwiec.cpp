#include "Leniwiec.h"
#include "Swiat.h"

CLeniwiec::CLeniwiec(CSwiat *swiat, int x, int y) : CZwierze(swiat, x, y)
{
	_symbol = 'L';
	_sila = 2;
	_inicjatywa = 1;
	_ruszalSieOstatnio = false;
}

CLeniwiec::~CLeniwiec(void)
{
}

void CLeniwiec::akcja() {
	if (getJestZywy() == false)
		return;
	if (_ruszalSieOstatnio == false)
	{
		_swiat->komunikat("Leniwiec postanowil sie ruszyc.");
		CZwierze::akcja();
		_ruszalSieOstatnio = true;
	}
	else
	{
		_swiat->komunikat("Leniwiec postanowil tym razem sie nie ruszac.");
		_ruszalSieOstatnio = false;
	}
}