#include "Guarana.h"
#include "Swiat.h"

CGuarana::CGuarana(CSwiat *swiat, int x, int y) : CRoslina(swiat, x, y)
{
	_symbol = 'G';
}

CGuarana::~CGuarana(void)
{
}

void CGuarana::kolizja(COrganizm *o) {
	o->setSila(o->getSila() + 3);
	_swiat->komunikat("Guarana zwieksza sile " + o->getName());
}