#include <cstdlib>
#include <ctime>

#include "Organizm.h"
#include "Swiat.h"

COrganizm::COrganizm(CSwiat *swiat, int x, int y)
{
	_symbol = '.';
	_wiek = 0;
	_swiat = swiat;
	_jestZywy = true;
	setXY(x, y);
	//srand(time(NULL));
}

COrganizm::~COrganizm(void)
{
}

void COrganizm::rysuj() {
	std::cout << " " << getSymbol();
}

void COrganizm::setXY(int x, int y) {
	_x = x;
	_y = y;
}

char COrganizm::getSymbol() {
	return _symbol;
}

int COrganizm::getInicjatywa() {
	return _inicjatywa;
}

int COrganizm::getSila() {
	return _sila;
}

void COrganizm::setSila(int sila) {
	_sila = sila;
}

bool COrganizm::isLegalMove(int x, int y) {
	return !(x >= 20 || x < 0 || y >= 20 || y < 0);
}

void COrganizm::sasiedniePola(int tab[][2]) {
	tab[0][0] = _x+1; tab[0][1] = _y;
	tab[1][0] = _x-1; tab[1][1] = _y;
	tab[2][0] = _x; tab[2][1] = _y+1;
	tab[3][0] = _x; tab[3][1] = _y-1;
	tab[4][0] = _x+1; tab[4][1] = _y+1;
	tab[5][0] = _x+1; tab[5][1] = _y-1;
	tab[6][0] = _x-1; tab[6][1] = _y+1;
	tab[7][0] = _x-1; tab[7][1] = _y-1;
}

int COrganizm::wybierzLosowePole() {
	int sasiednie[8][2];
	//std::cout << "\nPOLE (x,y)=(" << x  << ","<< y << "), symbol:" << symbol << std::endl;
	sasiedniePola(sasiednie);
	//srand(time(NULL));
	bool legalne = false;
	int pole = 0;
	while(legalne == false) {
		pole = rand() % 8;
		if (isLegalMove(sasiednie[pole][0], sasiednie[pole][1]))
			legalne = true;
	}
	return sasiednie[pole][1]*20 + sasiednie[pole][0];
}

int COrganizm::wybierzLosoweWolnePole()
{
	int sasiednie[8][2];
	sasiedniePola(sasiednie);

	std::list<int> wolne_pola;

	for (int i = 0; i < 8; i++)
	{
		if (isLegalMove(sasiednie[i][0], sasiednie[i][1]))
		{
			//sprawdza czy jest kolizja z organizmem, jesli nie to dodaje do listy wolnych pol
			if (_swiat->czyKolizja(sasiednie[i][0], sasiednie[i][1]) == false)
			{
				wolne_pola.push_back(sasiednie[i][0] + sasiednie[i][1]*20);
			}
		}
	}
	int ilosc = wolne_pola.size();
	int wynik = -1;
	if (ilosc > 0)
	{
		std::list<int>::iterator it = wolne_pola.begin();
		std::advance(it, rand() % ilosc);
		wynik = (*it);
	}

	return wynik;
}

int COrganizm::getPole() {
	return _x + 20 * _y;
}

int COrganizm::getX() {
	return _x;
}

int COrganizm::getY() {
	return _y;
}

void COrganizm::setSymbol(char symbol) {
	_symbol = symbol;
}

void COrganizm::setInicjatywa(int inicjatywa) {
	_inicjatywa = inicjatywa;
}

bool COrganizm::getJestZywy() {
	return _jestZywy;
}

void COrganizm::setJestZywy(bool jestZywy) {
	_jestZywy = jestZywy;
}

std::string COrganizm::getName() {
	switch (_symbol) {
	case 'C':
		return "Ciern";
		break;
	case 'G':
		return "Guarana";
		break;
	case 'U':
		return "gUziec";
		break;
	case 'L':
		return "Leniwiec";
		break;
	case 'O':
		return "Owca";
		break;
	case 'W':
		return "Wilk";
		break;
	case 'T':
		return "Trawa";
		break;
	case 'Z':
		return "Zolw";
		break;
	default:
		return "unknown";
		break;
	}
}

bool COrganizm::porownajWiek(COrganizm *a, COrganizm *b) {
	// zwraca true jesli a jest starszy od b
	return a->_wiek	> b->_wiek;
}

void COrganizm::postarz() {
	_wiek++;
}

int COrganizm::getWiek() {
	return _wiek;
}

bool COrganizm::porownajZyjace(COrganizm *a, COrganizm *b) {
	// zwraca true jesli a zyje
	return a->getJestZywy() > b->getJestZywy();
}