#pragma once

#include <exception>

#include "Swiat.h"
#include "Ciern.h"
#include "Guarana.h"
#include "Guziec.h"
#include "Leniwiec.h"
#include "Owca.h"
#include "Trawa.h"
#include "Wilk.h"
#include "Zolw.h"

namespace Fabryka {
	class CFabrykaOrganizmow
	{
	public:
		CFabrykaOrganizmow(void);
		~CFabrykaOrganizmow(void);
		static COrganizm* CFabrykaOrganizmow::utworzOrganizm(CSwiat *swiat, char typ, int x, int y);
		static COrganizm* CFabrykaOrganizmow::utworzOrganizm(CSwiat *swiat, int typ, int x, int y);

	private:
		enum { ORGANIZM, CIERN, GUARANA, TRAWA, GUZIEC, LENIWIEC, OWCA, WILK, ZOLW};
	};

	class tworzenieOrganizmuWyjatek : public std::exception
	{
		virtual const char* what() const throw()
		{
			return "Nie utworzono poprawnego organizmu!";
		}
	};
}