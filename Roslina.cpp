#include "Roslina.h"
#include "Swiat.h"

CRoslina::CRoslina(CSwiat *swiat, int x, int y) : COrganizm(swiat, x, y), _pRozmnazania(0.1)
{
	_sila = 0;
	_inicjatywa = 0;
}

CRoslina::~CRoslina(void)
{
}

void CRoslina::akcja()
{
	if (getJestZywy() == false)
		return;

	int losowa = rand() % 100;
	if (losowa < _pRozmnazania * 100)
	{
		int pole = wybierzLosoweWolnePole();
		if (pole != -1)
		{
			_swiat->dodajOrganizm(getSymbol(), pole % 20, pole / 20);
			_swiat->komunikat("Rozmnazanie organizmow typu: " + getName());
		}
		else return;
	}
}

void CRoslina::kolizja(COrganizm *o) {
}