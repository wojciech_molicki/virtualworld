#include "FabrykaOrganizmow.h"

Fabryka::tworzenieOrganizmuWyjatek twex;

Fabryka::CFabrykaOrganizmow::CFabrykaOrganizmow(void)
{
}

Fabryka::CFabrykaOrganizmow::~CFabrykaOrganizmow(void)
{
}

COrganizm* Fabryka::CFabrykaOrganizmow::utworzOrganizm(CSwiat *swiat, int typ, int x, int y) {
	//tu moze byc throwowanie custom wyjatku !
	try {
		switch (typ) {
		default:
			throw twex;
			break;
		case ORGANIZM:
			throw twex;
			break;
		case CIERN:
			return new CCiern(swiat, x, y);
			break;
		case GUARANA:
			return new CGuarana(swiat, x, y);
			break;
		case TRAWA:
			return new CTrawa(swiat, x, y);
			break;
		case GUZIEC:
			return new CGuziec(swiat, x, y);
			break;
		case LENIWIEC:
			return new CLeniwiec(swiat, x, y);
			break;
		case OWCA:
			return new COwca(swiat, x, y);
			break;
		case WILK:
			return new CWilk(swiat, x, y);
			break;
		case ZOLW:
			return new CZolw(swiat, x, y);
			break;
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return nullptr;
	}
}

COrganizm* Fabryka::CFabrykaOrganizmow::utworzOrganizm(CSwiat *swiat, char typ, int x, int y) {
	//tu moze byc throwowanie custom wyjatku !
	try {
		switch (typ) {
		default:
			throw twex;
			break;
		case 'C':
			return new CCiern(swiat, x, y);
			break;
		case 'G':
			return new CGuarana(swiat, x, y);
			break;
		case 'T':
			return new CTrawa(swiat, x, y);
			break;
		case 'U':
			return new CGuziec(swiat, x, y);
			break;
		case 'L':
			return new CLeniwiec(swiat, x, y);
			break;
		case 'O':
			return new COwca(swiat, x, y);
			break;
		case 'W':
			return new CWilk(swiat, x, y);
			break;
		case 'Z':
			return new CZolw(swiat, x, y);
			break;
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return nullptr;
	}
}