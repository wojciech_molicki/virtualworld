#pragma once

#include <iostream>
#include <list>
#include <string>

#include "Organizm.h"
#include "FabrykaOrganizmow.h"

class CSwiat
{
public:
	CSwiat();
	~CSwiat(void);

	void wykonajTure();
	void komunikat(std::string wiadomosc);
	COrganizm* czyKolizja(int x, int y);
	void dodajOrganizm(char typ, int x, int y);

private:
	static const int _startowaIloscOrganizmow = 20;
	static const int _maxInicjatywa = 8;
	int _turaNumer;
	std::list<std::string> _listaKomunikatow;
	std::list<COrganizm *> _organizmy;

	//indeks odpowiada inicjatywie
	std::list<COrganizm *> _listaInicjatyw[_maxInicjatywa];

	void rysujSwiat();
	void clearScreen();

	void rozmiescLosoweOrganizmy();
	void wypiszTure();
	void wyswietlKomunikaty();

	void akcjaOrganizmow();
	void przypiszInicjatywy();

	void posunCzas();

	void usunNiezywe();
};
