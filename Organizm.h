#pragma once

#define WINDOWS

#include <iostream>

class CSwiat;

class COrganizm
{
public:
	COrganizm(CSwiat *swiat, int x, int y);

	virtual ~COrganizm(void);
	virtual void rysuj();
	virtual void akcja() = 0;
	virtual void kolizja(COrganizm *o) = 0;

	std::string getName();

	int getX();
	int getY();
	int getPole();
	void setXY(int x, int y);

	int getSila();
	void setSila(int sila);

	char getSymbol();
	void setSymbol(char symbol);

	int getInicjatywa();
	void setInicjatywa(int inicjatywa);

	bool getJestZywy();
	void setJestZywy(bool jestZywy);

	static bool porownajWiek(COrganizm *a, COrganizm *b);
	static bool porownajZyjace(COrganizm *a, COrganizm *b);

	int getWiek();
	void postarz();

protected:
	CSwiat *_swiat;
	char _symbol;
	int _sila;
	int _inicjatywa;
	int wybierzLosowePole();
	int wybierzLosoweWolnePole();

private:
	int _wiek;
	bool _jestZywy;
	int _x;
	int _y;
	bool isLegalMove(int x, int y);
	void sasiedniePola(int tab[][2]);
};
