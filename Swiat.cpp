#include "Swiat.h"
#include <cstdlib>
#include <ctime>
#include <string>

CSwiat::CSwiat(void)
{
	_turaNumer = 0;
	clearScreen();
	rozmiescLosoweOrganizmy();
}

CSwiat::~CSwiat() {}

void CSwiat::rozmiescLosoweOrganizmy() {
	srand(static_cast<unsigned int>(time(NULL)));

	for (int i = 0; i < _startowaIloscOrganizmow; i++) {
		int randomPole = rand() % 400;
		int randomOrganizm = rand() % 8 + 1;
		int r_x = randomPole % 20; int r_y = randomPole / 20;
		_organizmy.push_back(Fabryka::CFabrykaOrganizmow::utworzOrganizm(this, randomOrganizm, r_x, r_y));
		//_organizmy.back()->_wiek = randomPole;
	}
}

void CSwiat::dodajOrganizm(char typ, int x, int y) {
	_organizmy.push_front(Fabryka::CFabrykaOrganizmow::utworzOrganizm(this, typ, x, y));
}

void CSwiat::przypiszInicjatywy() {
	for (std::list<COrganizm *>::const_iterator iterator = _organizmy.begin(), end = _organizmy.end(); iterator != end; ++iterator)
	{
		_listaInicjatyw[(*iterator)->getInicjatywa()].push_back((*iterator));
	}
}

void CSwiat::wykonajTure()
{
	//swiat rysowany jest przed posunieciem, wiec komunikaty pokazuja co sie stanie za chwile
	rysujSwiat();
	przypiszInicjatywy();
	akcjaOrganizmow();
	usunNiezywe();
	wyswietlKomunikaty();
	posunCzas();
}

void CSwiat::rysujSwiat() {
	if (_turaNumer != 0)
		clearScreen();
	std::cout << "                 WOJCIECH MOLICKI 126379\n";

	for (int i = 0; i < 20; i++) {
		for (int j = 0; j < 20; j++)
		{
			COrganizm *o_ptr = czyKolizja(j, i);
			if (o_ptr != nullptr)
				o_ptr->rysuj();
			else
				std::cout << ". " ;
		}
		std::cout << std::endl;
	}
	wypiszTure();
}

void CSwiat::akcjaOrganizmow() {
	for (int i = _maxInicjatywa - 1; i >=0; i--)
	{
		_listaInicjatyw[i].sort(COrganizm::porownajWiek);
		while (_listaInicjatyw[i].empty() == false)
		{
			// problem, kiedy jeden z organizmow zabije drugi i zostanie w liscie
			_listaInicjatyw[i].front()->akcja();
			_listaInicjatyw[i].pop_front();
		}
	}
}

void CSwiat::wyswietlKomunikaty() {
	std::string komunikat = "";
	while(_listaKomunikatow.empty() == false)
	{
		std::cout << _listaKomunikatow.back() << std::endl;
		_listaKomunikatow.pop_back();
	}
}

void CSwiat::komunikat(std::string wiadomosc) {
	_listaKomunikatow.push_front(wiadomosc);
}

void CSwiat::wypiszTure() {
	if (_turaNumer < 10)
		std::cout << "[                Tura " + std::to_string(_turaNumer) + "                ]\n";
	else if (_turaNumer < 100)
		std::cout << "[                Tura " + std::to_string(_turaNumer) + "               ]\n";
	else
		std::cout << "[               Tura " + std::to_string(_turaNumer) + "               ]\n";
}

void CSwiat::clearScreen() {
#ifdef WINDOWS
	std::system ( "CLS" );
#else
	std::system ( "clear" );
#endif
}

COrganizm* CSwiat::czyKolizja(int x, int y) {
	for (std::list<COrganizm *>::const_iterator iterator = _organizmy.begin(), end = _organizmy.end(); iterator != end; ++iterator)
	{
		if (x == (*iterator)->getX() &&  y == (*iterator)->getY())
		{
			return (*iterator);
		}
	}
	return nullptr;
}

void CSwiat::posunCzas() {
	for (std::list<COrganizm *>::const_iterator iterator = _organizmy.begin(), end = _organizmy.end(); iterator != end; ++iterator)
	{
		(*iterator)->postarz();
	}
	_turaNumer++;
}

void CSwiat::usunNiezywe() {
	_organizmy.sort(COrganizm::porownajZyjace);
	while(_organizmy.back()->getJestZywy() == false)
	{
		_organizmy.pop_back();
	}
}